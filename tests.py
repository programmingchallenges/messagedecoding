import unittest
import main

class TestStringMethods(unittest.TestCase):

    def testBasic(self):
        self.assertEqual(main.decode('111'), 3)

    def testRootOnly(self):
        self.assertEqual(main.decode('1'), 1)

    def test6CharacterMapping(self):
        self.assertEqual(main.decode('112211'), 13)

    def test6CharacterGreaterThan26Mapping(self):
        self.assertEqual(main.decode('3333333'), 1)

    def test4CharacterMapping(self):
        self.assertEqual(main.decode('1233'), 3)

if __name__ == '__main__':
    unittest.main()