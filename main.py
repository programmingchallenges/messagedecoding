import sys  

def decode(encodedString):
    charArray = list(encodedString)
    under26Count = 0
    for i in range(0, len(charArray)-1):
        if int(charArray[i]) * 10 + int(charArray[i+1]) < 27:
            under26Count += 1
    return Fibonacci(under26Count)

def Fibonacci(n): 
    if n==0: 
        return 1
    elif n==1: 
        return 2
    else: 
        return Fibonacci(n-1)+Fibonacci(n-2)


if __name__ == '__main__':
    print('No main for this file')
